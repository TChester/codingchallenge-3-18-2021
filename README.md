# CodingChallenge-3-18-2021

Okay! Your job is to implement the service and dao layers for this application AND create the database the dao will link to.

The main method should be all set to go; all you need to do it run it. Give the main method a look to see what the main method will do; BUT when I run the application there is no guarantee that I will use that EXACT main driver class. I'll use something similar at the very least.

Your DB will have THREE tables: Candy_Stores, Candy_Types, & Store_Type_Junction. The Candy_Store & Candy_Types tables only need to have a PK column and a "name" column. Also, the main method I provided assumes your primary keys will a SERIAL datatype in the DB.



