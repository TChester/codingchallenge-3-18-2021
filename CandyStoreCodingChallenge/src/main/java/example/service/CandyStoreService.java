package example.service;

import example.model.CandyStore;

import java.util.List;

public interface CandyStoreService {

    //CREATE
    public void addStore(CandyStore store);

    //READ
    public List<CandyStore> getAllStores();
    public CandyStore getStoreById(int id);

    //DELETE
    public void removeStore(int id);
}
