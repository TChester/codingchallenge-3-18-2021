package example.service;

import example.model.CandyType;

import java.util.List;

public interface CandyTypeService {
    //CREATE
    public void addCandy(CandyType candy);

    //READ
    public List<CandyType> getAllCandyTypes();
    public CandyType getCandyById(int id);

    //DELETE
    public void removeCandy(int id);
}
