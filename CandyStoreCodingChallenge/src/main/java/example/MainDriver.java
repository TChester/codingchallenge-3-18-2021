package example;

import example.model.CandyStore;
import example.model.CandyType;
import example.service.CandyStoreService;
import example.service.CandyStoreServiceImpl;
import example.service.CandyTypeService;
import example.service.CandyTypeServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class MainDriver {

    //this will start as an error, you shouldn't need to change this code; just fix the other files
    public static CandyStoreService candyS = new CandyStoreServiceImpl();
    public static CandyTypeService candyT = new CandyTypeServiceImpl();

    public static void main(String[] args) {

        //feel free to comment or uncomment this line
        initiallyDatabaseInserts();


        System.out.println("\n\n\nPrinting full list of candies");
        System.out.println(candyT.getAllCandyTypes());

        System.out.println("\n\n\nPrinting full list of stores");
        System.out.println(candyS.getAllStores());

        System.out.println("\n\n\nPrinting a specific candy");
        System.out.println(candyT.getCandyById(2));

        System.out.println("\n\n\nPrinting a specific store");
        System.out.println(candyS.getStoreById(1));

        System.out.println("\n\n\nDeleting a store");
        candyS.removeStore(2);

        ////REPRINT FULL LISTS
        System.out.println("\n\n\nPrinting full list of candies AGAIN");
        System.out.println(candyT.getAllCandyTypes());
        System.out.println("\n\n\nPrinting full list of stores AGAIN");
        System.out.println(candyS.getAllStores());

    }

    public static void initiallyDatabaseInserts(){
        //insert a list of candy types
        candyT.addCandy(new CandyType("sour gummy worms")); //Primary Key should be 1
        candyT.addCandy(new CandyType("gummy bears")); //Primary Key should be 2
        candyT.addCandy(new CandyType("nerds")); //Primary Key should be 3
        candyT.addCandy(new CandyType("skittles")); //Primary Key should be 4
        candyT.addCandy(new CandyType("twizzlers")); //Primary Key should be 5
        candyT.addCandy(new CandyType("lolipop")); //Primary Key should be 6



        //insert a list of candy stores
        List<Integer> candyList = new ArrayList<Integer>();
        candyList.add(1);
        candyList.add(2);
        candyList.add(5);
        candyS.addStore(
                new CandyStore("Sweet Tooth Candy Shop", candyList)); //Primary Key should be 1

        candyList = new ArrayList<Integer>();
        candyList.add(3);
        candyList.add(4);
        candyList.add(6);
        candyS.addStore(
                new CandyStore("Movie Theater Candy Shop", candyList)); //Primary Key should be 2

        candyList = new ArrayList<Integer>();
        candyList.add(5);
        candyS.addStore(
                new CandyStore("Natural Candy: Candy Shop", candyList)); //Primary Key should be 3

        candyList = new ArrayList<Integer>();
        candyList.add(1);
        candyList.add(3);
        candyS.addStore(
                new CandyStore("Old Timer's Candy Shop", candyList)); //Primary Key should be 4
    }


}
