package example.dao;

import example.model.CandyType;

import java.util.List;

public interface CandyTypeDao {
    //CREATE
    public void insertCandy(CandyType candy);

    //READ
    public List<CandyType> selectAllCandyTypes();
    public CandyType selectCandyById(int id);

    //DELETE
    public void deleteCandy(int id);
}
