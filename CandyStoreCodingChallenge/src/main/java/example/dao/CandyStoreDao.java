package example.dao;

import example.model.CandyStore;

import java.util.List;

public interface CandyStoreDao {
    //CREATE
    public void insertStore(CandyStore store);

    //READ
    public List<CandyStore> selectAllStores();
    public CandyStore selectStoreById(int id);

    //DELETE
    public void deleteStore(int id);
}
