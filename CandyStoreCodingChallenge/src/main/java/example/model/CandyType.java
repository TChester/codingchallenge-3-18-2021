package example.model;

public class CandyType {
    private int id;
    private String name;

    public CandyType(){

    }

    public CandyType(String name) {
        this.name = name;
    }

    public CandyType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "\nCandyType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
