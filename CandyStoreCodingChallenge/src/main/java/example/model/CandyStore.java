package example.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CandyStore {
    private int id;
    private String name;
    private List<Integer> selectionOfCandies; //a list of foreign key integers
        //there won't be a column "selectionOfCandies" in the Candy_Stores table; you may have to use joins OR
        // do some inserts into the junction table

    public CandyStore() {
        this.selectionOfCandies= new ArrayList<>(); //empty
    }

    public CandyStore(int id, String name) {
        this.id = id;
        this.name = name;
        this.selectionOfCandies= new ArrayList<>(); //empty
    }

    public CandyStore(String name, List<Integer> selectionOfCandies) {
        this.name = name;
        this.selectionOfCandies = selectionOfCandies;
    }

    public CandyStore(int id, String name, List<Integer> selectionOfCandies) {
        this.id = id;
        this.name = name;
        this.selectionOfCandies = selectionOfCandies;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getSelectionOfCandies() {
        return selectionOfCandies;
    }

    public void setSelectionOfCandies(List<Integer> selectionOfCandies) {
        this.selectionOfCandies = selectionOfCandies;
    }

    @Override
    public String toString() {
        return "\nCandyStore{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", selectionOfCandies=" + selectionOfCandies +
                '}';
    }
}
